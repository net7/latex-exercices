# $\LaTeX$ Exercices

## Installer une distribution $\TeX$

### Tex Live

Cette distribution est disponible sur quasiment toutes les distributions, il suffit alors de taper :
```bash
sudo apt install texlive
```

### MiKTeX *(pour Windaube)*

Commande pour winget :
```shell
winget install --id=MiKTeX.MiKTeX -e
```

### Overleaf, pour rester sur le navigateur

Y'a pas grand-chose à faire dans ce cas, il suffit d'aller sur [https://www.overleaf.com/](https://www.overleaf.com/) et c'est bon.


## Pouvoir écrire du $\LaTeX$

### Le compilateur

Vous venez d'installer une distribution $\TeX$ (sauf pour les utilisateurs d'Overleaf), il vous faut donc la surcouche $\LaTeX$.  
Pour ce faire, il existe des IDE et des commandes :

- la commande `pdflatex` permet la "compilation" d'un fichier $\LaTeX$ en un PDF
    - cf Liste des packages à installer
- [Texmaker](https://www.xm1math.net/texmaker/) est un IDE open-source qui intègre différentes méthodes de compilation, il est disponible sous Linux et Windows
- VSCode ça marche aussi
- Neovim avec [VimTeX](https://github.com/lervag/vimtex), il faut un poil de setup (et encore...) mais c'est pépite 👌

### Liste des packages à installer
Ubuntu :
```bash
sudo apt install texlive-latex-base texlive-latex-extra texlive-lang-french texlive-bibtex-extra biber
```
Arch :
```bash
pacman -S texlive-latexextra latexmk texlive-langfrench texlive-bibtexextra
```
Fedora : 
```bash
sudo dnf install texlive-latex texlive-ec texlive-a4wide texlive-babel-french texlive-comment texlive-wrapfig texlive-multirow
```

### Commandes pour compiler

Compiler un fichier tex en pdf :
```bash
pdflatex mon_fichier_latex.tex
```
Compiler les références :
```bash
biber mon_fichier_latex
```
Il faut lancer la compilation pdf **2** fois après `biber`.

## La pratique

### Introduction

**Objectif :** Compléter un document petit à petit pour avoir un exemple complet et modulable de toutes les petites fonctions intéressantes et utiles à la création d'un document $\LaTeX$.

Petit fil rouge de notre histoire : Vous êtes arrivé à l'ENSEEIHT et vous avez votre premier rapport à écrire en $\LaTeX$. Votre généreux professeur vous a donné ce squelete:
```latex
\documentclass[11pt,french,twoside]{article} % police taille 11 car 10 c'est pour les terroristes, twoside permet de différencier les pages paires et impaires (pour fancyhdr notamment)
\usepackage[french]{babel} % paquet qui permet de gérer les différentes langues
\usepackage{graphicx} % paquet pour inclure des images
\usepackage{fancyhdr} % permet de paramétrer les en-têtes et pieds de page

\begin{document}
\pagestyle{fancy}
\setlength{\headheight}{25.7pt} % pour supprimer les warnings latex
\addtolength{\topmargin}{-13.7pt} % pour supprimer les warnings latex
\fancyhead[CHE]{\textbf{JOLI LOGO} \includegraphics[width=20mm]{LaTeX_project_logo_bird.png}} % affiche "JOLI LOGO" et le logo de latex en centre (C) des en-têtes (H) de pages paires (E)
\fancyfoot[LO,RE]{AUCHERE Nathan} % affiche "AUCHERE Nathan" en pied de page à droite pour les pages paires (RE) et à gauche sur les pages impaires (LO)

\begin{figure}[t]
    \centering
    \includegraphics[width=5cm]{inp_n7.png} % joli logo de l'N7
\end{figure}

\title{\vspace{4cm} \textbf{Rapport de XXX}} 
\author{DUPONT Marc}
\date{\vspace{7cm} Département YYY - Première année \\
2023-2024 }

\maketitle

\newpage % génère un saut de page, en gros retour à la ligne mais pour une page
\tableofcontents % génère un sommaire des sections du document
\listoffigures % génère un sommaire des figures insérées dans le document
\newpage

\section{Introduction}
\section{Partie}
\section{Conclusion}

\end{document}
```
Un don particulièrement généreux, car c'est une très bonne base, modulaire et proprement commentée.

### Exercice 1 (le plus difficile)

Générer un fichier PDF à partir du squelette fourni. Pas besoin d'ajouter quoi que ce soit, il faut réussir à produire le PDF.
*C'est le moment où il faut crier à l'aide si vous avez des soucis !!!*

### Exercice 2

Il est grand temps d'écrire l'introduction de votre rapport. 
Au préalable, il faut remplir la page de garde avec les informations correctes :

- matière : au choix, mais par défaut ce seront les coléoptères
- NOM Prénom
- Département

Ensuite, il faut introduire le sujet. 
Vous avez déjà la syntaxe pour inclure une image, mais voici des compléments :

- changer le paramètre de taille : `width=`, `height=`, `scale=`
- changer le paramètre de placement (le [t] dans l'exemple) : 
    - **h** : placer l'image ici (approximativement)
    - **H** : requiert le paquet `float`, place l'image précisément à sa position dans le code $\LaTeX$
    - **t** : placer au sommet de la page
    - **b** : placer au bas de la page
- placer une image entourée de texte : paquet `wrapfig` => `\begin{wrapfigure}...\end{wrapfigure}`
- ajout d'une légende avec `\caption{...}`
- ajout d'un label pour les références avec `\label{fig:...}`

D'après votre prof il serait de bon goût de mettre en évidence les passages importants du texte avec de la mise en forme :

- **texte en gras** : `\textbf{...}`
- *texte en italique* : `\textit{...}` ou `\emph{}`
- <ins>texte souligné</ins> : `\underline{...}`
- nouvelle ligne : `\\` ou `\newline`

Vous pouvez aussi modifier les en-têtes et pieds de pages, vous pouvez lire la doc ici : [https://www.overleaf.com/learn/latex/Headers_and_footers#Using_the_fancyhdr_package](https://www.overleaf.com/learn/latex/Headers_and_footers#Using_the_fancyhdr_package)

### Exercice 3

Vous venez de citer des études. Il convient dès lors de faire référence à ces citations.  
Le paquet `biblatex`qui permet la gestion de bibliographie. Ce paquet se repose sur un fichier auxiliaire qu'il vous faudra écrire pour mettre en forme les références. On le spécifie avec la commande `\addbibresource{monfichier.bib}`.  
Pour écrire ce fichier, il y a une syntaxe spécifique. On concatène un ensemble de références qui suivent le schéma ci-contre :
```bash
@typeDocument{labelRef
    champ1 = valeur,
    champ2 = valeur,
    ...
}
```
Les champs disponibles sont très nombreux et je vous invite à en consulter la liste ici : [https://www.overleaf.com/learn/latex/Bibliography_management_in_LaTeX#Reference_guide](https://www.overleaf.com/learn/latex/Bibliography_management_in_LaTeX#Reference_guide)  
Pour afficher cette bibliographie, il existe la commande : `\printbibliography[...]`. plusieurs paramètres peuvent lui être fournis :

-  `\printbibliography[heading=bibintoc,title={Références}]` donne le titre "Référence" à la section de l'affichage et l'ajoute au sommaire
-  `\printbibliography[type=article,title={Articles}]` donne le titre "Articles" à la section et n'affichera que les références aux articles

Vous pouvez aussi (et surtout) faire le lien à une référence bibliographique au sein de votre document : `\cite{labelRef}`

### Exercice 4

On se concentre désormais sur le cœur du projet qui a donné lieu à ce rapport : l'étude sur l'évolution de la population des coccinelles dans la forêt d'Aizenay en Vendée. Vous écrirez ici le 1er développement, `\subsection` de votre étude, `\section`.  
Il apparaît que l'évolution de leur population suit la suite de Fibonacci. Il vous est demandé de la présenter comme suit :  
`$\left\lbrace\begin{array}{l}
    \text{u}_0 = 0\\
    \text{u}_1 = 1\\
    \text{u}_n = \text{u}_{n-1} + \text{u}_{n-2}
\end{array}\right.$`  

Voici les macros pour écrire des expressions mathématiques :

- `$expression$` permet d'écrire une expression "en ligne" c'est-à-dire comme ceci $x=y^2$ au milieu d'une ligne
- `$$expression$$` permet d'écrire l'expression complétement sur une ligne à part
- le bloc `array` permet d'écrire des matrices (dans mon exemple, j'ai écrit une matrice avec contenu aligné à gauche, 1 colonne et 3 lignes)
    - **s'écrit forcément dans une expression mathématique**
- les macros `\left\right` sont complémentaires et permettent d'étendre des accolades, crochets ou autre sur la hauteur de l'expression qui suit
    - `lbrace`...`rbrace` = {...}
    - `lbrack`...`rbrack` = [...]
    - `.` = vide (souvent utilisé pour compléter un right ou left comme dans l'exemple)
    - plus de documentation ici : [https://www.overleaf.com/learn/latex/Brackets_and_Parentheses#Reference_guide](https://www.overleaf.com/learn/latex/Brackets_and_Parentheses#Reference_guide)

### Exercice 5

Il vous a été demandé d'écrire un programme python pour calculer la population à une itération n. Vous le présenterez donc dans ce rapport.  
Le paquet `listing` permet d'écrire du code proprement dans un document $\LaTeX$. On peut lui passer en paramètres notamment :

- la macro `lstinputlisting` permet d'importer directement un fichier, sinon on écrit le code à la main entre `\begin{lstlisting}` et `\end{lstlisting}`
- `[numbers=left]` permet d'afficher le numéro de ligne de code à gauche
- `[language=Python]` permet d'écrire du python
- `\usepackage{matlab-prettifier}` permet d'importer le style MATLAB avec l'option `[style=Matlab-editor]`
- d'autres options sont disponibles dans la doc ici : [https://www.overleaf.com/learn/latex/Code_listing](https://www.overleaf.com/learn/latex/Code_listing)


### Exercice 6

Il est temps d'écrire la conclusion de votre rapport.  
Après avoir regroupé dans un tableau les informations importantes, donnez une petite phrase de conclusion.  
Pour faire un tableau (une table), il faut utiliser la macro `tabular`. Celle-ci prend en paramètre l'agencement des colonnes (ex : `{|c|c}` donne 2 colonnes à la table avec une ligne verticale à gauche de la 1ère et entre les 2 colonnes, mais pas après la deuxième) :

- `c` pour centrer
- `l` pour aligner à gauche
- `r` pour aligner à droite

On peut fusionner des colonnes à l'aide du paquet `multirow`. Il est possible alors de spécifier le nombre de colonnes à fusionner, l'agencement et le texte (ex : `\multicolumn{nbColonnes}{agencement}{texte}`).  
Il est aussi possible de tracer des lignes horizontales à l'aide de la commande `\hline`.  
Enfin, on peut colorer des lignes ou des colonnes à l'aide du paquet `xcolor`, pour un tableau, il faut l'import `\usepackage[table]{xcolor}` :

- des codes couleurs sont déjà spécifiés `green`, `gray`, `red`... on peut aussi donner un code hexadécimal avec `[HTML]{coleurEnHexa}`
- `\rowcolor{color}` permet de changer la couleur de la ligne à laquelle la commande est appelée
- `\newcolumntype{s}{>{\columncolor{couleur}} agencement}` permet de définir un nouveau type de colonne `s` ayant la couleur `couleur` et l'agencement `agencement`, il est alors possible d'ensuite l'utiliser dans la déclaration du `tabular`


### Fin mot de l'histoire

Vous savez maintenant écrire un document $\LaTeX$ tout à fait potable qui ne donnera pas envie à votre prof de vous étriper. Mais comme tout tutoriel, j'ai zappé beaucoup d'infos et il y a plein de trucs et astuces dépendants du formatage très spécifique que vous souhaitez appliquer.  
Je vous conseille très très très fortement de surfer un peu sur la doc d'Overleaf ([https://www.overleaf.com/learn](https://www.overleaf.com/learn)) qui est très complète et vous donnera l'immense majorité des solutions pour lesquelles vous pouvez opter, exemples à l'appui.  
Maintenant, amusez-vous bien avec vos rapports 🫡
