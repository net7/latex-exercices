# $\LaTeX$ Exercises

## Installing a $\TeX$ distribution

### Tex Live

This distribution is available on almost all distributions, so all you have to do is type :
```bash
sudo apt install texlive
```

### MiKTeX

Command for winget :
```shell
winget install --id=MiKTeX.MiKTeX -e
```

### Overleaf, to stay on the browser

There's not much to do in this case, just go to [https://www.overleaf.com/](https://www.overleaf.com/) and it's good.


## Being able to write $\LaTeX$

### The compiler

You have just installed a $\TeX$ distribution (except for Overleaf users), so you need the $\LaTeX$ overlay.  
There are IDEs and commands for this:

- the `pdflatex` command with `latexmk` allows you to "compile" a $\LaTeX$ file into a PDF
    - see List of packages to install
- [Texmaker](https://www.xm1math.net/texmaker/) is an open-source IDE that integrates various compilation methods. It is available for Linux and Windows.
- VSCode also works
- Neovim with [VimTeX](https://github.com/lervag/vimtex), requires a bit of setup but is a nugget 👌

### List of packages to install
Ubuntu :
```bash
sudo apt install texlive-latex-base texlive-latex-extra texlive-lang-english texlive-bibtex-extra biber
```
Arch:
```bash
pacman -S texlive-latexextra latexmk texlive-langfrench texlive-bibtexextra
```
Fedora : 
```bash
sudo dnf install texlive-latex texlive-ec texlive-a4wide texlive-babel-french texlive-comment texlive-wrapfig texlive-multirow
```

### Compiler commands

Compile a $\TeX$ file into a pdf :
```bash
pdflatex my_latex_file.tex
```
Compile references :
```bash
biber my_latex_file
```
Run pdf compilation **2** times after `biber`.

## Practicalities


### Introduction


**Objective:** To complete a document step by step so as to have a complete and modular example of all the interesting and useful little functions for creating a $\LaTeX$ document.


A little thread from our story: You've arrived at ENSSEIHT and you've got your first report to write in $\LaTeX$. Your generous teacher has given you this outline:
```latex
\documentclass[11pt,french,twoside]{article} % font size 11 because 10 is for terrorists, twoside to differentiate even and odd pages (for fancyhdr in particular)
\usepackage[french]{babel} % package to manage different languages
\usepackage{graphicx} % package to include images
\usepackage{fancyhdr} % allows you to configure headers and footers

\begin{document}
\pagestyle{fancy}
\setlength{headheight}{25.7pt} % to remove latex warnings
\addtolength{\topmargin}{-13.7pt} % to remove latex warnings
\fancyhead[CHE]{\textbf{JOLI LOGO} \includegraphics[width=20mm]{LaTeX_project_logo_bird.png}} % displays "JOLI LOGO" and the latex logo in the center (C) of the headers (H) of even-numbered pages (E)
\fancyfoot[LO,RE]{AUCHERE Nathan} % displays "AUCHERE Nathan" in right footer on even-numbered pages (RE) and left footer on odd-numbered pages (LO)

\begin{figure}[t]
    \centering
    \includegraphics[width=5cm]{inp_n7.png} % nice N7 logo
\end{figure}

\title{space{4cm} \textbf{Report by XXX}} 
\author{DUPONT Marc}
\date{space{7cm} Department YYY - First year
2023-2024 }

\maketitle

\newpage % generates a page break, basically a new line but for one page
\tableofcontents % generates a summary of document sections
\listoffigures % generates a summary of figures inserted in the document
\newpage

\section{Introduction}
\section{Part}
\section{Conclusion}

\end{document}
```
A particularly generous donation as this is a very good modular and neatly commented base.

### Exercice 1 (le plus difficile)

Generate a pdf file out of the provided outline. No need to add anything at all, you just have to manage to produce the pdf file.
*It's now time to scream for help if you have any problem !!!*


### Exercise 2


It's time to write the introduction to your report. 
First, fill in the cover page with the correct information:


- subject: your choice, but by default it will be beetles
- First name
- Department


Next, you need to introduce the subject. 
You already know the syntax for including an image, but here are some additions:


- change the size parameter: `width=`, `height=`, `scale=`
- change the placement parameter: 
    - **h**: place the image here (approximately)
    - **H**: requires the `float` package, places the image precisely at its position in the $\LaTeX$ code
    - **t** : place at the top of the page
    - **b** : place at the bottom of the page
- place an image surrounded by text: `wrapfig` package => `begin{wrapfigure}...\end{wrapfigure}` package
- add a caption with `caption{...}`.
- add a label for references with ``label{fig:...}`.


According to your teacher, it would be in good taste to highlight important passages in the text using formatting:


- **bold text**: `\textbf{...}`
- *italicised text*: `textit{...}` or `emph{}`.
- <ins>underlined text</ins> : `underline{...}` or
- new line : ``newline`` or ``newline``.

### Exercise 3


You have just quoted some studies. You need to refer to these citations.  
The `biblatex` package allows you to manage bibliographies. This package relies on an auxiliary file which you will have to write to format the references. This is specified with the command `addbibresource{myfile.bib}`.  
There is a specific syntax for writing this file. We concatenate a set of references which follow the following pattern :
```bash
@typeDocument{labelRef
    field1 = value,
    field2 = value,
    ...
}
```
There are a large number of fields available, and I invite you to consult the list here: [https://www.overleaf.com/learn/latex/Bibliography_management_in_LaTeX#Reference_guide](https://www.overleaf.com/learn/latex/Bibliography_management_in_LaTeX#Reference_guide)  
To display this bibliography there is the command: `printbibliography[...]`. Several parameters can be supplied to it:


- `printbibliography[heading=bibintoc,title={References}]` gives the title "References" to the display section and adds it to the table of contents
- `printbibliography[type=article,title={Articles}]` gives the title "Articles" to the section and will only display references to articles.


You can also (and more importantly) link to a bibliographic reference within your document: `\cite{labelRef}`.

### Exercise 4


We are now concentrating on the core of the project which gave rise to this report: the study of the evolution of the ladybird population in the Aizenay forest in the Vendée. You will write here the 1st development, `subsection` of your study, `section`.  
It appears that the evolution of their population follows the Fibonacci sequence. You are asked to present it as follows:  
$\left\lbrace\begin{array}{l}
    \text{u}_0 = 0
    \text{u}_1 = 1
    \text{u}_n = \text{u}_{n-1} + \text{u}_{n-2}
\end{array}\right.$  


Here are the macros for writing mathematical expressions:


- `$expression$` allows you to write an expression "in line", i.e. like this $x=y^2$ in the middle of a line
- `$$expression$$` allows you to write the entire expression on a separate line
- the `array` block is used to write matrices (in my example, I wrote a matrix with contents aligned on the left, 1 column and 3 rows)
    - **is necessarily written in a mathematical expression**.
- the `left\right` macros are complementary and allow you to extend braces, square brackets or other elements over the height of the following expression
    - `lbrace`...`rbrace` = {...}
    - `lbrack`...`rbrack` = [...]
    - `.` = empty (often used to complete a right or left as in the example)
    - more documentation here: [https://www.overleaf.com/learn/latex/Brackets_and_Parentheses#Reference_guide](https://www.overleaf.com/learn/latex/Brackets_and_Parentheses#Reference_guide

### Exercise 5


You have been asked to write a python program to calculate the population at iteration n. You will therefore present it in this report.  
The `listing` package allows you to write code neatly in a $\LaTeX$ document. You can pass it the following parameters


- the `lstinputlisting` macro is used to import a file directly, otherwise you write the code by hand between `begin{lstlisting}` and `end{lstlisting}`.
- `[numbers=left]` displays the line number of the code on the left.
- '[language=Python]` allows you to write python
- `usepackage{matlab-prettifier}` allows you to import the MATLAB style with the `[style=Matlab-editor]` option.
- other options are available in the doc here : [https://www.overleaf.com/learn/latex/Code_listing](https://www.overleaf.com/learn/latex/Code_listing)


### Exercise 6


It's time to write the conclusion to your report.  
After grouping the important information in a table, write a short concluding sentence.  
To create a table, use the `tabular` macro. This takes as parameters the arrangement of the columns (e.g. `{|c|c}` gives the table 2 columns with a vertical line to the left of the 1st column and between the 2 columns but not after the second) :


- `c` to center
- `l` to align left
- `r` to align right


You can merge columns using the `multirow` package. You can then specify the number of columns to be merged, the layout and the text (eg `multicolumn{nbColumns}{layout}{text}`).  
Horizontal lines can also be drawn using the `hline` command.  
Finally, you can colour lines or columns using the `xcolor` package. For a table, you need to import `usepackage[table]{xcolor}` :


- colour codes are already specified `green`, `gray`, `red`... you can also give a hex code with `[HTML]{colourEnHexa}`.
- `rowcolor{color}` allows you to change the colour of the line where the command is called up
- `newcolumntype{s}{>{columncolor{colour}} layout}` is used to define a new column type `s` with the colour `colour` and layout `layout`, which can then be used in the tabular declaration.


### End of story


You now know how to write a perfectly drinkable $\LaTeX$ document that won't make your teacher want to gut you. But like any tutorial, I've skipped a lot of information and there are a lot of tips and tricks depending on the very specific formatting you want to apply.  
I very, very, very strongly advise you to have a look at the Overleaf doc ([https://www.overleaf.com/learn](https://www.overleaf.com/learn)) which is very comprehensive and will give you the vast majority of the solutions you can opt for, with examples to back it up.  
Now, enjoy your reports 🫡
